package com.javagda22.dziedziczenie.zad1;

public class Samochod {
    protected double predkosc;
    protected boolean swiatlaWlaczone;

    public void przyspiesz() {
        if (predkosc > 110.0) {
            predkosc = 120.0;
            System.out.println("Zwiększam prędkość do " + predkosc);
        } else {
            predkosc += 10.0;
        }
    }

    public void wlaczSwiatla() {
        swiatlaWlaczone = true;
    }

    public boolean czySwiatlaWlaczone() {
        return swiatlaWlaczone;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "predkosc=" + predkosc +
                ", swiatlaWlaczone=" + swiatlaWlaczone +
                '}';
    }
}
