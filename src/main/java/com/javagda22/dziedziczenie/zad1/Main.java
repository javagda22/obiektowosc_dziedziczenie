package com.javagda22.dziedziczenie.zad1;

public class Main {
    public static void main(String[] args) {
        Samochod samochod = new Samochod();
        samochod.przyspiesz();
        samochod.przyspiesz();
        samochod.przyspiesz();
        samochod.przyspiesz();
        samochod.przyspiesz();
        System.out.println(samochod);


        Kabriolet kabriolet = new Kabriolet();
        kabriolet.przyspiesz();
        kabriolet.schowajDach();
        kabriolet.przyspiesz();
        System.out.println(kabriolet);

        Samochod takitamSobie = new Kabriolet();
//        takitamSobie.schowajDach(); // nie jest możliwe, ponieważ
        // schowajDach pochodzi z klasy Kabriolet, która nie jest dostępna
        // w klasie samochód, a 'takitamSobie' samochód jest typu
        // (jest zadeklarowany jako) Samochód.

//        Kabriolet kabrio = new Samochod(); // nie jest możliwe
        // nie każdy samochód jest kabrioletem.
    }
}
