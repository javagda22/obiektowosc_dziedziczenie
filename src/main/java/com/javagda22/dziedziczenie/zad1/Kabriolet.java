package com.javagda22.dziedziczenie.zad1;

// rozszerzanie == dziedziczenie
public class Kabriolet extends Samochod{
    private boolean dachSchowany;

    public void schowajDach(){
        dachSchowany = true;
    }

    public boolean czyDachSchowany() {
        return dachSchowany;
    }

    @Override
    public String toString() {
        return "Kabriolet{" +
                "dachSchowany=" + dachSchowany +
                ", predkosc=" + predkosc +
                ", swiatlaWlaczone=" + swiatlaWlaczone +
                '}';
    }
}
