package com.javagda22.dziedziczenie.zad2;

public class Main {
    public static void main(String[] args) {
//        Ptak ptak = new Ptak();
//        ptak.śpiewaj();
//
//        Kukulka kukulka = new Kukulka();
//        kukulka.śpiewaj();
//
//        Sowa sowa = new Sowa();
//        sowa.śpiewaj();
//
//        Bocian bocian = new Bocian();
//        bocian.śpiewaj();

//         polimorfizm / wielopostaciowość
        Ptak ptak = new Bocian();
//        ptak.śpiewaj(); // kle kle
        // D R Y
        // DONT REPEAT YOURSELF

        Ptak[] ptaki = new Ptak[5];
        ptaki[0] = new Ptak();
        ptaki[1] = new Bocian();
        ptaki[2] = new Kukulka();
        ptaki[3] = new Sowa();
        ptaki[4] = new Kukulka();

        for (int i = 0; i < ptaki.length; i++) {
            ptaki[i].śpiewaj(); // każdy ptak ma zaśpiewać

            if(ptaki[i] instanceof Kukulka) { // sprawdzam czy któryś
                                // z nich jest kukułką
                Kukulka kukulka = (Kukulka) ptaki[i]; // jeśli tak
                kukulka.stuk();             // dokonuję rzutowania
                // i wywołuję metodę 'stuk()'
            }
        }
    }
}
