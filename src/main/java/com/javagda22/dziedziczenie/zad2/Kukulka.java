package com.javagda22.dziedziczenie.zad2;

public class Kukulka extends Ptak {
    @Override
    public void śpiewaj() {
        System.out.println("ku ku"); // ku ku
        super.śpiewaj(); // ćwir ćwir
    }

    public void stuk(){
        System.out.println("Stuk");
    }
}
