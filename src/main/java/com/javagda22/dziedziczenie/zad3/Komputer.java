package com.javagda22.dziedziczenie.zad3;

public class Komputer {
    private String producent;
    private double moc;
    private TypProcesora typProcesora;

    public Komputer(String producent, double moc, TypProcesora typProcesora) {
        this.producent = producent;
        this.moc = moc;
        this.typProcesora = typProcesora;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public double getMoc() {
        return moc;
    }

    public void setMoc(double moc) {
        this.moc = moc;
    }

    public TypProcesora getTypProcesora() {
        return typProcesora;
    }

    public void setTypProcesora(TypProcesora typProcesora) {
        this.typProcesora = typProcesora;
    }
}
