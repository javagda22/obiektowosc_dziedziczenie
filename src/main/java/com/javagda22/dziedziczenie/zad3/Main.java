package com.javagda22.dziedziczenie.zad3;

public class Main {
    public static void main(String[] args) {
        Komputer komputer = new Komputer("a", 1, TypProcesora.JEDNORDZENIOWY);
        komputer.setMoc(2.0);
        komputer.setProducent("Intel");
        komputer.setTypProcesora(TypProcesora.WIELORDZENIOWY);

    }
}
