package com.javagda22.dziedziczenie.zad3;

public class Laptop extends Komputer {
    private double wielkoscMatrycy;
    private boolean posiadaRetine;

    public Laptop(String producent, double moc, TypProcesora typProcesora, double wielkoscMatrycy, boolean posiadaRetine) {
        super(producent, moc, typProcesora);
        // wywołanie super musi być pierwszą instrukcją

        this.wielkoscMatrycy = wielkoscMatrycy;
        this.posiadaRetine = posiadaRetine;
    }
}
